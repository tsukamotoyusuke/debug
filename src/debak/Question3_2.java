package debak;

public class Question3_2 {

	public static void main(String args[]) {
		String str1 = "Harmonize";
		String str2 = "Harmo";
		String str3 = str2 + "nize";

		boolean result = stringComparison(str1, str3);
		System.out.println(result);
	}

	public static boolean stringComparison(String str1, String str3) {

		if(str1 == str3) {
			return true;
		} else if (str1 != str3) {
			return false;
		}
		return true;
	}
}
